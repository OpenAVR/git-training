Advanced Git Operations
=======================

Tagging
-------

.. index:: tags, git-tag

|REFS|

 * https://git-scm.com/book/en/v2/Git-Basics-Tagging

.. todo:: Discuss tagging (annotated vs. non-annotated).

Cherry Picking Commits
----------------------

.. index:: cherry picking, git-cherry-pick

|REFS|

* https://git-scm.com/book/en/v2/Distributed-Git-Maintaining-a-Project#_rebase_cherry_pick
* https://git-scm.com/book/en/v2/Appendix-C%3A-Git-Commands-Patching

.. todo:: Discuss cherry picking commits.

Reverting Commits
-----------------

.. index:: revert, git-revert

|REFS|

* https://git-scm.com/book/en/v2/Git-Tools-Advanced-Merging#_reverse_commit
* https://git-scm.com/book/en/v2/Appendix-C%3A-Git-Commands-Patching

.. todo:: Discuss reverting commits.

Aliases
-------

.. index:: aliases

You can create *aliases* for |git| commands that you use often to save you some
typing.

I have added the following aliases to my :file:`~/.gitconfig` file:

.. code-block:: ini

    [alias]
        st = status
        br = branch
        co = checkout
        ci = commit -s

Aliases are optional. Use them if you find them useful.

More info::

    $ git help config                # Search for 'alias'

|REFS|

* https://git-scm.com/book/en/v2/Git-Basics-Git-Aliases

Generating and Applying Patches
-------------------------------

.. index:: git-format-patch, format-patch, git-am, am, generating patches, applying patches

|REFS|

* https://git-scm.com/book/en/v2/Distributed-Git-Maintaining-a-Project#_patches_from_email
* https://git-scm.com/book/en/v2/Distributed-Git-Contributing-to-a-Project#_project_over_email
* https://git-scm.com/book/en/v2/Appendix-C%3A-Git-Commands-Email

.. todo:: Discuss ``git format-patch`` and ``git am``.

.. _mergetool-config-setup-label:

Diff Tool and Merge Tool Configuration
--------------------------------------

.. index:: diff tool, merge tool

Using a diff tool or a merge tool is almost required when viewing complicated
diffs or attempting to resolve a difficult merge conflict. Trying to resolve a
difficult merge manually in your editor is madness and will likely result in an
incorrect merge. Merge conflicts can be hard to resolve, so do yourself a favor
and spent the time to find a merge tool that works for you and learn how to use
it. You will be happy that you did.

Various diff tool and merge tool options are available:

* kdiff3_
* vimdiff_
* meld_
* kompare_
* xxdiff_

.. _kdiff3: http://kdiff3.sourceforge.net/
.. _vimdiff: http://vimdoc.sourceforge.net/htmldoc/diff.html
.. _meld: http://meldmerge.org/
.. _kompare: http://www.caffeinated.me.uk/kompare/
.. _xxdiff: http://furius.ca/xxdiff/

There are many other options, but the above are the ones that I have tried.

Having tried all of the above, I have found :command:`kdiff3` to work the best
for me.

To configure |git| to use :command:`kdiff3` when running :command:`git
mergetool` or :command:`git difftool`, I added the following to my
:file:`~/.gitconfig` file:

.. code-block:: ini

    [merge]
        tool = kdiff3
    [mergetool]
        keepBackup = false
    [mergetool "kdiff3"]
        cmd = /usr/bin/kdiff3 $BASE $LOCAL $REMOTE -o $MERGED
        trustExitCode = false
    [diff]
        tool = kdiff3
        renames = copies
    [difftool "kdiff3"]
        cmd = /usr/bin/kdiff3 $LOCAL $REMOTE

If you wish to use another tool, this may work with some tweaking.

Splitting Directories out of a Repository
-----------------------------------------

.. index:: splitting repositories, subtree split

.. todo:: Show how to split a repo in to multiple repos using :command:`git
          subtree split`.

Working with multiple Repositories
----------------------------------

.. index:: multi repo tools

More often than not, you will be working on a product that is built from
multiple git repositories (Google Android is an extreme example of this).
Fortunately, there are tools that can help make working with many repositories
easier.

Google Repo
^^^^^^^^^^^

.. index:: Google Repo

Google Repo makes cloning and managing a group of git repositories easier
through the use of an XML manifest file. The manifest file lives in its own git
repository which can be branched and tagged to represent a branching and
tagging of the group as a whole.

    https://code.google.com/archive/p/git-repo/

Using Google Repo:

    https://source.android.com/setup/develop/repo

If you need to write or maintain a manifest file, you'll need to read this:

    https://gerrit.googlesource.com/git-repo/+/master/docs/manifest-format.md

Here's an example project which uses a Google Repo manifest file:

    https://github.com/openavr-org/imx7-base-manifest

Git Multi
^^^^^^^^^

.. index:: git-multi

The :command:`git-multi` is an enhancement to |git| which allows you to run a
|git| command on multiple repositories at the same time. Particularly useful
for me are the following::

    $ git multi status
    $ git multi grep 'some-stuff'

My implementation of :command:`git-multi` knows how to use Google Repo manifest
files to determine which repos to operate on, although it can also work without
Google Repo:

    https://github.com/openavr-org/git-openavr

Additional Resources
====================

After finishing this training, you will likely be interested in some
additional resources related to Git.

Git Web Site
------------

The Git Web Site has loads of information:

    http://git-scm.com/

Pro Git Book
------------

.. index:: Pro Git Book

The Pro Git book is freely available online:

    https://git-scm.com/book

Git Repository Hosting Services
-------------------------------

.. index:: hosting services

There are many cloud based services for git repositories. The following are the
big three. There are many more features of each service than provided here, do
your own research to see which suits your needs best.

|gitlab|_
^^^^^^^^^

.. index:: GitLab

https://gitlab.com/

* Free public and private repositories.
* GitLab Pages can be used to host docs: https://about.gitlab.com/features/pages/

|github|_
^^^^^^^^^

.. index:: GitHub

https://github.com/

* Free for public repositories.
* Must pay for private repositories.
* GitHub Pages are a great way to host websites.

|bitbucket|_
^^^^^^^^^^^^

.. index:: BitBucket

https://bitbucket.org/

* Free public and private repositories.
* Need to pay if you need to give more than five people access to a private
  repo.
* Great Jira integration since Atlassian runs both services.

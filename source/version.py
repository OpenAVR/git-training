import subprocess

version_cmd = ['git', 'describe', '--dirty', '--long']

RELEASE = subprocess.check_output(version_cmd).decode().strip()
VERSION = RELEASE.split('-')[0]

if __name__ == '__main__':
    print('Version: {}'.format(VERSION))
    print('Release: {}'.format(RELEASE))

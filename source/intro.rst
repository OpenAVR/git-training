.. _intro-label:

Introduction
============

Git should be treated as a tool that makes your life as a developer easier and
allows you to try new ideas without risking the integrity of your code base.
Use of |git|, or any other source control tool, should not be treated as unnecessary
paperwork or an afterthought. Use |git| for everything you do such that it
becomes second nature. The mastery of |git| is a skill that all developers should
attain.

This training is meant to be a hands on, exercise based introduction
to |git|. |git| is a source code management tool that is widely used in
the IT industry.

This document will *not* cover the following:

* Using |git| on Windows (although command line |git| via bash works).
* Using |git| from a GUI tool or from an IDE as I believe mastery of |git| on
  the command line is far more powerful and portable than what most GUI tools can
  provide (the may be some GUI tools out that are quite powerful, I don't use GUI
  tools, other than |gitk|, so I probably speak from ignorance here).

Objectives
----------

* Create an empty local |git| repo.
* Make changes within your local repo.
* Working with branches in your local repo.
* Clone a remote repo.
* Publish your changes to a remote repo.
* Fetch/Merge/Pull changes from a remote repo.
* Collaborate with other developers.
* Select and negate commits using :command:`git-revert` and
  :command:`git-cherry-pick`.

The Pro Git Book
----------------

.. index:: Pro Git Book

Much of the information for this document came directly from the |ProGitBook|_.

Reading this book from cover to cover and understanding it all will put you
well on the way to becoming an expert git user.

Before going any further with this document, you are strongly encouraged to
read the following chapters of the |ProGitBook|_:

* `Chapter 1: Getting Started
  <https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control>`_
* `Chapter 2: Git Basics
  <https://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository>`_
* `Chapter 3: Git Branching
  <https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell>`_
* `Appendix C: Git Commands
  <https://git-scm.com/book/en/v2/Appendix-C%3A-Git-Commands-Setup-and-Config>`_

If you do that, you may not need this document, but it is hoped that this
document will be a good companion to the book.

Also recommended reading after you have mastered the basics:

* `Chapter 5: Distributed Git
  <https://git-scm.com/book/en/v2/Distributed-Git-Distributed-Workflows>`_
* `Chapter 7: Git Tools
  <https://git-scm.com/book/en/v2/Git-Tools-Revision-Selection>`_
  (some of these topics are touched in this document)

Reference links to the |ProGitBook|_ are provided when relevant throughout this
document.

Contributing to the Document
----------------------------

.. index:: contributing

This is an open source document with the source project hosted on |gitlab|_

    https://gitlab.com/OpenAVR/git-training

Feel free to use the issue tracker there to report problems or to request
improvements to the document. Please include the version string, shown at the
top and bottom of each page of the html, in the issue so that we can better
determine how to address the issue.

If you want to help improve the document, please fork it and send merge
requests.

Viewing the Document
--------------------

This automatically generated html is viewable:

    http://openavr.gitlab.io/git-training/

A PDF version of the document is available for download:

    http://openavr.gitlab.io/git-training/GitTraining.pdf

These are automatically updated when changes are pushed to the master branch of
the source project.

LICENSE
-------

.. index:: license

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
International License. To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative
Commons, PO Box 1866, Mountain View, CA 94042, USA.

.. image:: img/cc-by-sa-88x31.png
   :alt: Creative Commons License
   :align: center

.. _prereqs-label:

Prerequisites
-------------

There are a few things you need to do to prepare for the training.

* Install git tools on your system.

  * |git|
  * |gitk|
  * a diff tool (:command:`kdiff3`, :command:`meld`, :command:`xxdiff`, etc)
  * others???

* Create a user account on |gitlab|_, |github|_, or |bitbucket|_ (or any git
  hosting service you want to use; your company might host a service internally
  that you can use to collaborate with your co-workers).

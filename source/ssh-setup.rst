.. _ssh-setup-label:

SSH Setup
=========

.. index:: ssh setup

In order to access a git service using git, you will need to create an ssh key
pair and install it on the git service.

The following examples will use |gitlab| as an example. Other services may be
slightly different in some respects, but the overall theory should be similar.

Generating a SSH key
--------------------

.. index:: ssh keys, generating ssh keys

If you have not created a ssh key, use the following command to
generate one (do this from your workstation so you will have the key
available for use after the training)::

    $ cd ~/.ssh
    $ ssh-keygen -t ed25519 -f id_ed25519_git

Some git hosting services may not yet support ``ed25519`` keys. If the service
you want to use does not support ``ed25519`` keys, generate your key pair with
the following::

    $ ssh-keygen -t rsa -b 4096 -f id_rsa_git

.. note::

   ECC (Eliptic Curve Cryptography) keys are much smaller and less
   computationally intensive than RSA keys, while being equally or more secure.
   For RSA keys to become more secure, you need to increase the number of bits
   which increases both the size and the computational load to use the key.  Also,
   it appears that the future of cryptographic keys will likely be ECC. With that
   in mind, you should probably start using ``ed25519`` keys when ever possible.

   Reference:

       https://wiki.archlinux.org/index.php/SSH_keys

Next, will you need to upload the public key (:file:`id_ed25519_git.pub`) to
the git repository hosting service.

.. note::

   It is not required to name your key :file:`id_ed25519_git`. Doing so only
   makes it clear to you what the key is used for. Any ssh key that you have
   already created can be used if you do not want to create another key. The
   only requirement is that a public key identifying you is installed on the
   hosting service's server.

.. note::

   There is nothing stopping you from using the same ssh key for every server
   you need access to, but best practices dictate that you should generate a
   different key for each server.

|REFS|

* https://git-scm.com/book/en/v2/Git-on-the-Server-Generating-Your-SSH-Public-Key

Upload Public Key to |gitlab|
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. index:: GitLab: installing ssh keys

Login to |gitlab| and install the contents of the public key file using this
page:

    https://gitlab.com/profile/keys

Test the key installation with (you should get your user name in the response
instead of mine)::

    $ ssh -i ~/.ssh/id_ed25519_git git@gitlab.com
    Enter passphrase for key '/home/troth/.ssh/id_ed25519_git':
    PTY allocation request failed on channel 0
    Welcome to GitLab, Theodore A. Roth!
    Connection to gitlab.com closed.

Upload Public Key to |github|
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. index:: GitHub: installing ssh keys

Login to |github| and install the contents of the public key file using this
page:

    https://github.com/settings/keys

Test the key installation with::

    $ ssh -i ~/.ssh/id_ed25519_git git@github.com
    Enter passphrase for key '/home/troth/.ssh/id_ed25519_git':
    PTY allocation request failed on channel 0
    Hi troth! You've successfully authenticated, but GitHub does not provide shell access.
    Connection to github.com closed.

Upload Public Key to |bitbucket|
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. index:: BitBucket: installing ssh keys

Login to |bitbucket| and install the contents of the public key file using this
page:

    https://bitbucket.org/account/user/your-user-id/ssh-keys/

Test the key installation with::

    $ ssh -i ~/.ssh/id_ed25519_git git@bitbucket.org
    Enter passphrase for key '/home/troth/.ssh/id_ed25519_git':
    PTY allocation request failed on channel 0
    logged in as taroth.

    You can use git or hg to connect to Bitbucket. Shell access is disabled.
    Connection to bitbucket.org closed.

Adding an SSH Config Entry
--------------------------

.. index:: ssh config, config: ssh

You can make your life a little easier by adding an entry for the hosting
service to your :file:`~/.ssh/config` file.

Add the following to your :file:`~/.ssh/config` file on your
workstation::

    $ cat >>~/.ssh/config <<EOF
    Host gitlab
         User git
         Hostname gitlab.com
         Port 22
         Identityfile ~/.ssh/id_ed25519_git
    EOF

Add similar entries for |github| or |bitbucket| should you wish to use those
services.

With this configuration entry in place, you can now just use the following to
test access to the service::

    $ ssh gitlab
    Enter passphrase for key '/home/troth/.ssh/keys/id_ed25519_git':
    PTY allocation request failed on channel 0
    Welcome to GitLab, Theodore A. Roth!
    Connection to gitlab.com closed.

.. index:: ssh-agent

.. |ssh| replace:: :command:`ssh`
.. |ssh-agent| replace:: :command:`ssh-agent`

SSH Agent
---------

When pushing and pulling from a remote |git| repository, |git| will use |ssh|
to both authenticate you to the server and to  encrypt the data passed between
your system and the server.

Each time a |git| command communicates with the remote server, you will be
asked to provide the pass phrase for your |ssh| key. This can get annoying and
tedious quite quickly. There are two ways to avoid having to type a pass phrase
every time:

* Generate an |ssh| key with no pass phrase. This is simple to do but is *bad
  practice*. If someone obtains your private key they can immediately use it
  to access your |git| repositories on the server (e.g. they could insert
  malicious code, delete branches, or any number of bad operations). Even
  worse, if you use the same key for many servers, they would have access to
  all of them. It is *not* recommended to use |ssh| keys with no pass phrase.

* Use |ssh-agent|. This tool will unlock a key by having you enter the
  pass phrase for the key once and continue to use that unlocked key for the
  duration of the session, or until the |ssh-agent| process is terminated.

On many systems, a |ssh-agent| is automatically started when you login to the
desktop. You can check if a |ssh-agent| is running with the following::

    $ env | grep SSH_AGENT
    SSH_AGENT_PID=25422

If ``SSH_AGENT_PID`` is set, then |ssh-agent| is likely running. You can verify
with the following::

    $ ps ax | grep 'ssh-agen[t]'
    25422 ?        Ss     0:00 ssh-agent
    26904 ?        Ss     0:00 ssh-agent

Notice that on my system, I have two instances of |ssh-agent| running and one
matches the ``SSH_AGENT_PID`` value. There is nothing wrong with running
multiple instances of |ssh-agent|. One could have been started automatically
when you logged in on the desktop and another could have been started by you
via an |ssh| login from a remote system.

If you need to start the |ssh-agent|, the following is the easiest way
(assuming you are running in a ``bash`` shell)::

    $ eval $(ssh-agent)
    Agent pid 26904

which is equivalent to this::

    $ ssh-agent
    SSH_AUTH_SOCK=/tmp/ssh-zkl1xk7JkO7W/agent.26903; export SSH_AUTH_SOCK;
    SSH_AGENT_PID=26904; export SSH_AGENT_PID;
    echo Agent pid 26904;
    $ SSH_AUTH_SOCK=/tmp/ssh-zkl1xk7JkO7W/agent.26903; export SSH_AUTH_SOCK;
    $ SSH_AGENT_PID=26904; export SSH_AGENT_PID;
    $ echo Agent pid 26904;
    Agent pid 26904

.. note::

   Starting |ssh-agent| from the command line will make that instance of
   |ssh-agent| accessible only from within that shell session, not from other
   shells you have started from your desktop environment. You could export the
   variables as done above in other shells to share the |ssh-agent| with other
   shells currently running.

Once you have an instance of |ssh-agent| running, you will need to add keys to the agent::

    $ ssh-add ~/.ssh/id_ed25519_gitlab
    Enter passphrase for /home/troth/.ssh/id_ed25519_gitlab:
    Identity added: /home/troth/.ssh/id_ed25519_gitlab (troth@example)

You can view which keys have been added to the agent with::

    $ ssh-add -l
    256 SHA256:WDXiVEhLF7Lt053cqvTXpxb62u+4Uv9e/sYnd9bEYno troth@example (ED25519)

Now any |git| commands (accessing the remote server via |ssh|) run from this
shell or session will no longer need to have you enter the pass phrase to
unlock the |ssh| key.

Of course, the |ssh-agent| is useful beyond the realm of |git|. Use it for any
|ssh| or :command:`scp` operations.
